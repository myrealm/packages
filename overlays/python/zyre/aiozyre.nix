{ buildPythonPackage
, fetchPypi
, pytest
, cython
, zeromq
, czmq
, zyre
}:
buildPythonPackage rec {
  pname = "aiozyre";
  version = "1.1.3";

  src = fetchPypi {
    inherit pname version;
    sha256 = "1ln7dnfmk2p2nks0z5cv3kc33fsw1mgcp5m8sq9v224czpzlv1y6";
  };

  buildInputs = [ cython zeromq czmq zyre ];
  propagatedBuildInputs = [ cython ];

  #doCheck = false;
}
