{ buildPythonPackage
, fetchPypi
, scikit-build
, c-blosc
, cmake
, python
, numpy
, psutil
}:
buildPythonPackage rec {
  pname = "blosc";
  version = "1.9.2";

  src = fetchPypi {
    inherit pname version;
    sha256 = "1gvll65458a0nzjdjk6frr2pl33v4kp3b6524zq3cn0328hnl6c9";
  };

  checkInputs = [ cmake numpy psutil ];
  buildInputs = [ cmake scikit-build ];
  #propagatedBuildInputs = [ c-blosc ];

    #${python.interpreter} setup.py build_clib
  buildPhase = ''
    ${python.interpreter} setup.py build_ext --inplace
  '';

  doCheck = true;
}
