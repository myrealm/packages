{ buildPythonPackage
, fetchPypi
, pyzmq
, aiozyre
, ujson
, aiohttp
, psycopg2
, paho-mqtt
, zepl-device
, fetchFromGitLab
}:
buildPythonPackage rec {
  pname = "gilgamesh";
  version = "0.99.999";

  #src = fetchPypi {
  #  inherit pname version;
  #  sha256 = "2vl7p9r1f2wrsn83wp7d7gaj4mgxa0a1npak7n6hfzldxljvwzff";
  #};

  propagatedBuildInputs = [ pyzmq aiozyre aiohttp ujson psycopg2 paho-mqtt ];

  src = fetchFromGitLab {
    owner = "leonardp";
    repo = "ggm";
    rev = "master";
    sha256 = "0z1qpwxinwlnnl9gqnzl8i8z56h5y1dn98y6qfx0vfn18yl0sbgj";
  };
  doCheck = false;
}
