{ lib, python3Packages, cmsis-pack-manager }:

python3Packages.buildPythonApplication rec {
  pname = "pyocd";
  version = "0.32.0";
  #version = "0.31.0";

  src = python3Packages.fetchPypi {
    inherit pname version;
    sha256 = "1lgaamjzlh61ygfpnm690c5zd31gvirvw907qlx3flgj620lkanm";
    #sha256 = "00hc59zkz7m2z5hmshr4k4kk678ldlsjn8lywp2caa492j4l5vhj";

  };

  nativeBuildInputs = [
    python3Packages.setuptools-scm-git-archive
    python3Packages.setuptools-scm
    cmsis-pack-manager
  ];

  propagatedBuildInputs =
    with python3Packages;
  [
    pypemicro
    pyocd-pemicro
    pyyaml
    prettytable
    pyusb
    six
    capstone
    pylink-square-10
    #pylink-square
    cmsis-pack-manager
  ];

  #doCheck = false;
}

