{ lib
, rustPlatform
, python3Packages
, cmsis-pack-manager
}:
#{ buildPythonPackage
#, fetchPypi
#, python3Packages
#, rustPlatform
#, setuptools-rust
#}:
#buildPythonPackage rec {
python3Packages.buildPythonApplication rec {
  pname = "cmsis-pack-manager";
  version = "0.3.0";

  src = python3Packages.fetchPypi {
    inherit pname version;
    sha256 = "017162ahwhz9jla3mywlf2rar7c7ypqy9bx9h7ps0wx5r0v52rc2";
  };

  patches = [ ./Cargo.lock.patch ];

  cargoRoot = "rust";

  cargoDeps = rustPlatform.fetchCargoTarball {
    inherit patches src;
    sourceRoot = "${pname}-${version}/${cargoRoot}";
    name = "${pname}-${version}";
    sha256 = "1igxilhja8d9mzv2skpzpgi90p5nl76x53b58nds5i7i8r2w44f9";
  };

  #nativeBuildInputs = [ setuptools-rust ] ++ (with rustPlatform; [
  nativeBuildInputs = [ ] ++ (with rustPlatform; [
    cargoSetupHook
    cargoBuildHook
    rust.cargo
    rust.rustc
  ]);

  propagatedBuildInputs = with python3Packages; [
    pyyaml
    appdirs
    milksnake
  ];
  doCheck = false;
}
