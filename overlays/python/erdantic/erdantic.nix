{ buildPythonPackage
, fetchPypi
, importlib-metadata
, pydantic
, pygraphviz
, typer
, typing-extensions
}:
buildPythonPackage rec {
  pname = "erdantic";
  version = "0.4.0";

  src = fetchPypi {
    inherit pname version;
    hash = "sha256-xpvP4NoWaQpKY+5UXANhFlU2jWdhtxBPIr2PHxkFRJM=";
  };

  propagatedBuildInputs = [
    importlib-metadata
    pydantic
    pygraphviz
    typer
    typing-extensions
  ];
  doCheck = false;
}
