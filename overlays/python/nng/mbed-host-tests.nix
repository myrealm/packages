{ buildPythonPackage
, fetchFromGitHub
, mbed-os-tools
}:
buildPythonPackage rec {
  pname = "mbed-host-tests";
  version = "1.8.11";

  src = fetchFromGitHub {
    owner = "ARMmbed";
    repo = "mbed-os-tools";
    rev = "aa029e57e6f945be81f5333e247ae40255c4e6c9";
    hash = "sha256-M89w8Ki4aZqmS2+WonG8Jj6uZX3MJ8HNGmqfQ9OM/kI=";
  };
  sourceRoot = "source/packages/mbed-host-tests/";
  propagatedBuildInputs = [ mbed-os-tools ];

  doCheck = false;
}
