{ buildPythonPackage
, fetchPypi
, pyserial
, requests
, intelhex
, prettytable
, fasteners
, appdirs
, junit-xml
, six
, beautifulsoup4
, lockfile
, future
, colorama
}:
buildPythonPackage rec {
  pname = "mbed-os-tools";
  version = "0.0.15";

  src = fetchPypi {
    inherit pname version;
    hash = "sha256-NQx7kTqjfFAZqitvwim+wIEhK2WSuNekpXH9TKzmEOY=";
  };

  propagatedBuildInputs = [ pyserial requests intelhex prettytable fasteners appdirs junit-xml six colorama beautifulsoup4 lockfile future ];

  doCheck = false;
}
