{ buildPythonPackage
, fetchPypi
, nng
, pytest-runner
, cmake
, cffi
, mbedtls
, pytestrunner
, sniffio
}:
buildPythonPackage rec {
  pname = "pynng";
  version = "0.7.1";

  #src = fetchPypi {
  #  inherit pname version;
  #  hash = "sha256-Xt08y+2iJk11DYFTOhHUW177+Nmdpluh9FaWNvDrm3A=";
  #};
  src = /home/leo/hobby/nanomsg/pynng;

  buildInputs = [ cffi pytest-runner nng ];
  #nativeBuildInputs = [ cffi pytest-runner ];
  propagatedBuildInputs = [ cffi sniffio ];

  doCheck = false;
}
