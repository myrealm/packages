self: super:
let
  #unstable = import <nixpkgs-unstable> {};
  pyOverlay = {
    packageOverrides = py-self: py-super: {

      zepl-device = py-self.callPackage ./zepl/zepl-device.nix {};
      aiozyre = py-self.callPackage ./zyre/aiozyre.nix {};

      #zepl-broker = py-super.callPackage ./zepl/zepl-broker-pkg.nix {};

      #gilgamesh = super.callPackage ./gilgamesh/gilgamesh.nix {};

      smbus2 = py-self.callPackage ./smbus2.nix {};

      #python-blosc = py-self.callPackage ./gilgamesh/python-blosc.nix {};

      base45 = py-self.callPackage ./base45.nix {};

      # pynng
      pynng = py-self.callPackage ./nng/pynng.nix {};
      pytest-curio = py-self.callPackage ./nng/pytest-curio.nix {};
      mbed-os-tools = py-self.callPackage ./nng/mbed-os-tools.nix {};
      mbed-host-tests = py-self.callPackage ./nng/mbed-host-tests.nix {};

      #graphviz = unstable.python3Packages.graphviz;
      erdantic = py-self.callPackage ./erdantic/erdantic.nix {};

      pyelftools = py-super.pyelftools.overridePythonAttrs (oldAtrrs: rec {
        doCheck = false;
      });

      # pyOCD stuff
      pypemicro = py-self.callPackage ./pyocd/pypemicro.nix {};
      pyocd-pemicro = py-self.callPackage ./pyocd/pyocd-pemicro.nix {};
      pyusb = py-super.pyusb.overridePythonAttrs (oldAtrrs: rec {
        pname = "pyusb";
        version = "1.2.1";
        src = py-super.fetchPypi {
          inherit pname version;
          sha256 = "1fg7knfzybzija2b01pzrzhzsj989scl12sb2ra4f503l8279k54";
        };
      });
      pylink-square-10 = py-self.callPackage ./pyocd/pylink-square-10.nix {};
      #cmsis-pack-manager = py-self.callPackage ./pyocd/cmsis-pack-manager.nix {};
      # y not?!?!?
      #pylink-square = py-super.pylink-square.overridePythonAttrs (oldAtrrs: rec {
      #  pname = "pylink-square";
      #  version = "0.10.1";
      #  src = super.fetchFromGitHub {
      #    owner = "square";
      #    repo = "pylink";
      #    rev = "v${version}";
      #    sha256 = "1q5sm1017pcqcgwhsliiiv1wh609lrjdlc8f5ihlschk1d0qidpd";
      #  };
      #});
    };
  };
  #py2Overlay = {
  #  packageOverrides = py-self: py-super: {
  #    certifi = py-self.callPackage ./certifi {};
  #  };
  #};
in
{
  #python2 = super.python3.override py2Overlay;
  python3 = super.python3.override pyOverlay;
  #python38 = super.python38.override pyOverlay;

  # python applications >>> top level application
  zepl = super.callPackage ./zepl/zepl.nix {};
  zepl-broker = super.callPackage ./zepl/zepl-broker.nix {};
  pyocd = super.callPackage ./pyocd/pyocd.nix {};
  cmsis-pack-manager = super.callPackage ./pyocd/cmsis-pack-manager.nix {};
}
