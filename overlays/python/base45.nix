{ buildPythonPackage
, fetchPypi
}:
buildPythonPackage rec {
  pname = "base45";
  version = "0.4.3";

  src = fetchPypi {
    inherit pname version;
    hash = "sha256-kVL/m1DrPGQDrq+/TY21DWKVxAu1jxV33a+LR0FRw4g=";
  };

  #buildInputs = [ cython ];
  #propagatedBuildInputs = [ cython ];

  doCheck = true;
}
