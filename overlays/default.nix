final: prev:
rec
{
  pyOverlay = prev.python3.override {
    packageOverrides = py-self: py-super: {

      zepl-device = py-self.callPackage ./python/zepl/zepl-device.nix {};
      aiozyre = py-self.callPackage ./python/zyre/aiozyre.nix {};

      #zepl-broker = py-super.callPackage ./python/zepl/zepl-broker-pkg.nix {};

      #gilgamesh = super.callPackage ./python/gilgamesh/gilgamesh.nix {};

      smbus2 = py-self.callPackage ./python/smbus2.nix {};

      #python-blosc = py-self.callPackage ./python/gilgamesh/python-blosc.nix {};

      base45 = py-self.callPackage ./python/base45.nix {};

      # pynng
      pynng = py-self.callPackage ./python/nng/pynng.nix {};
      pytest-curio = py-self.callPackage ./python/nng/pytest-curio.nix {};
      mbed-os-tools = py-self.callPackage ./python/nng/mbed-os-tools.nix {};
      mbed-host-tests = py-self.callPackage ./python/nng/mbed-host-tests.nix {};

      erdantic = py-self.callPackage ./python/erdantic/erdantic.nix { };

      pyelftools = py-super.pyelftools.overridePythonAttrs (oldAtrrs: rec {
        doCheck = false;
      });

      # pyOCD stuff
      pypemicro = py-self.callPackage ./python/pyocd/pypemicro.nix {};
      pyocd-pemicro = py-self.callPackage ./python/pyocd/pyocd-pemicro.nix {};
      pyusb = py-super.pyusb.overridePythonAttrs (oldAtrrs: rec {
        pname = "pyusb";
        version = "1.2.1";
        src = py-super.fetchPypi {
          inherit pname version;
          sha256 = "1fg7knfzybzija2b01pzrzhzsj989scl12sb2ra4f503l8279k54";
        };
      });
      pylink-square-10 = py-self.callPackage ./python/pyocd/pylink-square-10.nix {};
      #cmsis-pack-manager = py-self.callPackage ./pyocd/cmsis-pack-manager.nix {};
      # y not?!?!?
      #pylink-square = py-super.pylink-square.overridePythonAttrs (oldAtrrs: rec {
      #  pname = "pylink-square";
      #  version = "0.10.1";
      #  src = super.fetchFromGitHub {
      #    owner = "square";
      #    repo = "pylink";
      #    rev = "v${version}";
      #    sha256 = "1q5sm1017pcqcgwhsliiiv1wh609lrjdlc8f5ihlschk1d0qidpd";
      #  };
      #});
    };
  };
  # python packages overlay
  #python3 = prev.python3.override pyOverlay;
  #python3 = prev.python3.pkgs;
  python3Packages = pyOverlay.pkgs;
  #python38 = prev.python38.override pyOverlay;

  # python applications >>> top level
  #zepl = final.callPackage ./python/zepl/zepl.nix { python3Packages=python3Packages; };
  #zepl = prev.callPackage ./python/zepl/zepl.nix { python3Packages=python3Packages; zepl-device=python3Packages.zepl-device;};
  zepl = prev.callPackage ./python/zepl/zepl.nix {};
  zepl-broker = final.callPackage ./python/zepl/zepl-broker.nix {};

  pyocd = prev.callPackage ./python/pyocd/pyocd.nix {};
  cmsis-pack-manager = prev.callPackage ./python/pyocd/cmsis-pack-manager.nix {};

  # non-python packages
  zyre = prev.callPackage ./zyre.nix { };
  nanomq = prev.callPackage ./nanomq.nix { };
  nng = prev.callPackage ./nng.nix { };
  unbound-adblock = prev.callPackage ./unbound-adblock.nix { };
  phppgadmin = prev.callPackage ./phppgadmin.nix { };
}
