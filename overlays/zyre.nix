{ stdenv
, lib
, fetchFromGitHub
, cmake
, zeromq
, czmq
, libsodium
, asciidoc
}:
stdenv.mkDerivation rec {
  pname = "zyre";
  version = "2.0.0";

  src = fetchFromGitHub {
    owner = "zeromq";
    repo = "zyre";
    rev = "ab8dec907be554aff718620f4299e8e0b0830207";
    hash = "sha256-myYzHbjJf1A8M6YrnCShSjNBE6CQ+LmZ1q0dJEfAFIg=";
  };

  nativeBuildInputs = [ cmake asciidoc libsodium zeromq czmq ];

  enableParallelBuilding = true;

  doCheck = false; # fails all the tests (ctest)

  meta = with lib; {
    branch = "latest_release";
    homepage = https://github.com/zeromq/zyre;
    description = "A Framework for Distributed Computing";
    license = licenses.mpl20;
    platforms = platforms.all;
  };
}

