{
  description = "packages as overlays";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/23.05";
    unstable.url = "github:NixOS/nixpkgs/nixos-unstable";
  };

  ##
  ## also see:
  ## https://github.com/Mic92/sops-nix/blob/master/flake.nix
  ## this seems to be a weird way to do all of this
  ##

  outputs = { self, nixpkgs, unstable, flake-utils, ... }@inputs:
    let
      #overlay = final: prev: import ./overlays;
      overlay = import ./overlays;
      pkgsForSystem = system: import nixpkgs {
        overlays = [
          overlay
          self.overlays.default
        ];
        inherit system;
      };
      pkgs = import nixpkgs {
        overlays = [ self.overlays.default ];# overlay ];
      };

    in
    flake-utils.lib.eachSystem [ "x86_64-linux" "aarch64-linux" ] (system: rec {
    #flake-utils.lib.eachDefaultSystem (system: rec {
      checks = {
        inherit (packages);#nix-search;
      };

      #packages = pkgsForSystem;
      packages =
        let pkgs = import nixpkgs {
          inherit system;
          overlays = [ self.overlays.default ];# overlay ];
        };
        in {
          inherit (pkgs);#nix-search;
        };

      #legacyPackages = pkgsForSystem system;

      apps = {
        zyre = {
          type = "app";
          program = "${pkgs.myrealm.zyre}/bin/zpinger";
          #program = "${pkgs.unstable.hello}/bin/hello";
        };
        zepl = {
          type = "app";
          program = "${pkgs.myrealm.zepl}/zepl";
        };
      };
      #defaultApp = apps.zyre;

    }) // {
      overlays.default = final: prev: {

        unstable = import unstable {
          system = final.system;
          #overlays = [ overlay ];
        };

        #myrealm = import ./overlays { system = final.system; };
        #myrealm = overlay { system = final.system; };
        myrealm = import nixpkgs {
          overlays = [ overlay ];
          system = final.system;
          #inherit (final.system);
        };
      };
    };
}
